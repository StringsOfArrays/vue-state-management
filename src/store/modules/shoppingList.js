import List from '../../../data/List.json';

export default {
  namespaced: true,
  state: {
    shoppingList: List,
  },
  mutations: {
    addItem(state, { item }) {
      const newItem = { id: state.shoppingList.length + 1, ...item };
      state.shoppingList.push(newItem);
    },
    removeItem(state, { item }) {
      const idx = state.shoppingList.findIndex((elem) => elem.id === item.id);
      state.shoppingList.splice(idx, 1);
    },
  },
  getters: {
    getShoppingList(state) {
      return state.shoppingList;
    },
  },
  actions: {
    add(context, { item }) {
      context.commit('addItem', { item });
    },
    remove(context, { item }) {
      context.commit('removeItem', { item });
    },
  },
};
