import Vue from 'vue';
import VueRouter from 'vue-router';
import ShoppingList from '../views/ShoppingList.vue';
import AddItem from '../views/AddItem.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'ShoppingList',
    component: ShoppingList,
  },
  {
    path: '/add',
    name: 'AddItem',
    component: AddItem,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
